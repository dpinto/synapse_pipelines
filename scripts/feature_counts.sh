#!/bin/sh

# Reset module system for node architecture
module purge
unset MODULEPATH
source /etc/profile.d/modules.sh

# Check if we have a module system
if type "module" >/dev/null 2>&1;
then
   # Load modules
   module load python/2.7.6
   module load py_packages
   module load subread/1.5.0-p1
fi

# Run featurecounts
featureCounts -p -t exon -s 2 -g gene_id -a ~/lncRNA/RNAseq-pipeline-databases/hg19-v19-ERCC/gencode.v19.cabili.refseq.annotation.ERCC.gtf -o $2 $1
