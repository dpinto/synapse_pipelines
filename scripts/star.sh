#!/bin/sh

# Reset module system for node architecture
module purge
unset MODULEPATH
source /etc/profile.d/modules.sh

# Check if we have a module system
if type "module" >/dev/null 2>&1;
then
   # Load modules
   module load python/2.7.6
   module load py_packages
   module load STAR/2.5.2b
fi

# Run cutadapt
cutadapt -f fastq -m 32 -e 0.15 -O 6 -q 20 --quality-base=33 \
         -a AGATCGGAAGAGCGGTTCAGCAGGAATGCCGAGACCGATCTCGTATGCCGTCTTCTGCTT -a AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT -a AGATCGGAAGAGCACACGTCT \
         -A AGATCGGAAGAGCGGTTCAGCAGGAATGCCGAGACCGATCTCGTATGCCGTCTTCTGCTT -A AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT -A AGATCGGAAGAGCACACGTCT \
         -o $1.qc.fastq \
         -p $2.qc.fastq \
         $1.fastq.gz $2.fastq.gz \
         > $3.cutadapt.log 2>&1
pigz -p 6 -b 2048 $1.qc.fastq
pigz -p 6 -b 2048 $2.qc.fastq

# Run STAR
STAR \
        --chimSegmentMin 15  \
        --chimJunctionOverhangMin 15 \
        --outSAMstrandField None \
        --genomeDir /sc/orga/projects/pintod02b/RNAseq-pipeline-databases/hg19-v19-ERCC/star \
        --sjdbGTFfile /sc/orga/projects/pintod02b/RNAseq-pipeline-databases/hg19-v19-ERCC/gencode.v19.cabili.refseq.annotation.ERCC.tophat.gtf \
        --runThreadN 6 \
        --twopassMode Basic \
        --outFilterType BySJout \
        --outFilterMultimapNmax 20 \
        --alignSJoverhangMin 8 \
        --alignSJDBoverhangMin 1 \
        --sjdbScore 1 \
        --outFilterMismatchNmax 999 \
        --alignIntronMin 20 \
        --alignIntronMax 1000000 \
        --alignMatesGapMax 1000000 \
        --outReadsUnmapped Fastx \
        --outFilterMismatchNoverReadLmax 0.04 \
        --outStd SAM \
        --outSAMmode Full \
        --outSAMattributes NH HI AS NM MD \
        --outSAMtype BAM SortedByCoordinate \
        --outSAMunmapped Within \
        --outSAMattrRGline ID:0 LB:Lib_UMB5242_CBL SM:UMB5242_CBL PL:Illumina \
        --quantMode TranscriptomeSAM \
        --outWigType bedGraph \
        --outWigNorm RPM \
        --outWigStrand Stranded \
        --outFileNamePrefix $3 \
        --readFilesCommand zcat \
        --readFilesIn $1.qc.fastq.gz $2.qc.fastq.gz
